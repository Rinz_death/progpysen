<!DOCTYPE html>
  <html lang="en-gb" dir="ltr" class="uk-notouch">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>ProgPySen</title>
      <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="{{  url_for('static', filename='css/uikit.css') }}"  media="screen,projection"/>
         <link type="text/css" rel="stylesheet" href="{{  url_for('static', filename='css/uikit.almost-flat.css') }}"  media="screen,projection"/>
{#        <link type="text/css" rel="stylesheet" href="{{  url_for('static', filename='css/style.css') }}"/>#}
        {# blogs.html #}
{#        <link href="{{  url_for('static', filename='css/jquery.bxslider.css') }}" rel="stylesheet" />#}
        {# cabinet.html #}
{#        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">#}
        {# fotogallerey.html #}
{#        <link href="{{  url_for('static', filename='css/jquery.fancybox.css') }}" rel="stylesheet" />#}
        <script   src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
        <script   src="{{  url_for('static', filename='js/uikit.min.js') }}"></script>
        <link type="text/css" rel="stylesheet" href="{{  url_for('static', filename='css/codemirror/lib/codemirror.css') }}"  media="screen,projection"/>
        <script   src="{{  url_for('static', filename='css/codemirror/lib/codemirror.js') }}"> </script>
        <script   src="{{  url_for('static', filename='css/codemirror/mode/markdown/markdown.js') }}"> </script>
        <script   src="{{  url_for('static', filename='css/codemirror/addon/mode/overlay.js') }}"> </script>
        <script   src="{{  url_for('static', filename='css/codemirror/mode/xml/xml.js') }}"> </script>
        <script   src="{{  url_for('static', filename='css/codemirror/mode/gfm/gfm.js') }}"> </script>
{#        <link rel="stylesheet" href="{{  url_for('static', filename='js/jquery.fancybox-buttons.css') }}">#}
{#        <link rel="stylesheet" href="{{  url_for('static', filename='js/jquery.fancybox-thumbs.css') }}">#}
        {# lk-messages.html #}
{#        <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" />#}
{#        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>#}
{#          <script type=text/javascript>#}
{#                  $SCRIPT_ROOT = {{ request.script_root|tojson|safe }};#}
{#          </script>#}
      <!--Let browser know website is optimized for mobile-->
    </head>