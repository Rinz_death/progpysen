{% for i in news %}
    <div class="uk-width-1-1">
        <div class="uk-panel uk-panel-box">
            <p class="uk-margin-remove uk-text-truncate">{{ i.title }}</p>
        </div>
    </div>
{% endfor %}