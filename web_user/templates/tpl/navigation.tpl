{#<button style="height: 3%; width:3%;" class="uk-button" data-uk-offcanvas="{target:'#my-id'}">-</button>#}
<div id="my-id" class="uk-offcanvas uk-active">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-show" >
        <div class="uk-panel">
            <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
                <li class="uk-nav-divider"></li>
                <li class="uk-nav-header">ProgPySen</li>
                <li class="uk-nav-divider"></li>
                <li><a>Главная</a></li>
                <li><a href="/news">Новости</a></li>
                <li><a href="/posts">Статьи</a></li>
                <li><a href="/video-materials">Видео-материал</a></li>
                <li class="uk-nav-divider"></li>
                <li class="uk-nav-header">Личный кабинет</li>
                <li class="uk-nav-divider"></li>
                <li><a href="/login">{% if session['x_key'] %} Вход {% else %}Авторизация {% endif %}</a></li>
                {% if session['x_key'] %}
                    <li><a href="/lk/message">Сообщения</a></li>
                    <li><a href="/lk">Уведомления</a></li>
                    <li><a>Выход</a></li>
                {% endif %}
            </ul>
        </div>
    </div>
</div>