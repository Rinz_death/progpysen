{% for i in news %}
    <div class="uk-width-medium-1-2">
        <div class="uk-panel uk-panel-box">
            <div class="uk-panel-badge uk-badge">{{ i.theme }}</div>
            <h2 class="uk-panel-title">{{ i.title|safe }}</h2>
            <div class="uk-panel-teaser">
                <img src="{{ url_for('static', filename='img/'+i.image) }}" alt="">
            </div>
            <p>{{ i.information|safe }}</p>
        </div>
    </div>
{% endfor %}