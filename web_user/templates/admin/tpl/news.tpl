<div class="tm-main uk-width-medium-3-4">
    <article class="uk-article">
        <h1 class="uk-article-title">Создание новостей</h1>
        <div class="uk-block uk-block-secondary uk-contrast">
            <form class="uk-form" method="post">
                <fieldset data-uk-margin>
                    <div class="uk-container">
                        <div class="uk-grid uk-grid-match">
                            <div class="uk-width-medium-1-2 uk-row-first">
                                <div class="uk-panel">
                                    <h5>Заголовок</h5>
                                    <input type="text" name="title">
                                    <h5>Дата новости</h5>
                                    <input type="date" name="date">
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2 uk-row-first">
                                <div class="uk-panel">
                                    <h5>Картинка на главной странице</h5>
                                    <div class="uk-form-file">
                                        <input type="file">
                                    </div>
                                    <h5>Текст</h5>
                                    {# <textarea data-uk-htmleditor="{mode:'tab'}">...</textarea> #}
                                    <textarea name="text">...</textarea>
                                </div>
                            </div>

                            <button class="uk-button uk-align-center" type="submit">Добавить</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </article>
</div>
{% for i in news %}
    <div class="uk-width-medium-1-2">
        <div class="uk-panel uk-panel-box">
            <div class="uk-panel-badge uk-badge">{{ i.theme }}</div>
            <h2 class="uk-panel-title">{{ i.title|safe }}</h2>
            <div class="uk-panel-teaser">
                <img src="{{ url_for('static', filename='img/'+i.image) }}" alt="">
            </div>
            <p>{{ i.information|safe }}</p>
        </div>
    </div>
{% endfor %}
<script type="application/javascript">
    var htmleditor = UIkit.htmleditor(textarea, { /* options */ });
</script>