{#<button style="height: 3%; width:3%;" class="uk-button" data-uk-offcanvas="{target:'#my-id'}">-</button>#}
<div id="my-id" class="uk-offcanvas uk-active" style="width: 14%; ">
    <div class="uk-offcanvas-bar uk-offcanvas-bar-show" mode="push">
        <div class="uk-panel">
            <ul class="uk-nav uk-nav-offcanvas uk-nav-parent-icon" data-uk-nav>
                <li class="uk-nav-divider"></li>
                <li class="uk-nav-header">Административная панель</li>
                <li class="uk-nav-divider"></li>
                <li><a href="/">Главная</a></li>
                <li><a href="/admin/settings/news">Управление новостями</a></li>
                <li><a href="/admin/settings/post_or_video_post">Управление материалами</a></li>
                <li><a href="/admin/settings/users">Управление пользователями</a></li>
                <li><a href="/admin/info/statistics">Все статистики</a></li>
                <li><a href="/admin/exit">Деактивироваться</a></li>
            </ul>
        </div>
    </div>
</div>