<div class="uk-width-medium-1-1" style="padding-top: 20%;">
    <div class="uk-panel uk-panel-box uk-flex-center">
        <h3 class="uk-panel-title">Авторизация</h3>
        <p> {{ auth }}</p>
        <fieldset data-uk-margin>
            <form class="uk-form" method="post">
                {# Login #}
                <div class="uk-flex-center">
                    <input name="email" placeholder="Почта" type="text" class="uk-width-1-2">
                </div>
                {# Pass #}
                <div class="uk-flex-center">
                    <div class="uk-form-password">
                        <input name="password" placeholder="Пароль" type="password" class="uk-width-1-2">
                    </div>
                </div>
                {# Роль #}
                <div class="uk-flex-center">
                   <div class="uk-button uk-form-select uk-width-1-2" data-uk-form-select>
                       <span class="">Ваша роль</span>
                       <select name="role">
                           <option value="1">Модератор</option>
                           <option value="2">Администратор</option>
                       </select>
                   </div>
                </div>
                <button class="uk-button" type="submit">Авторизоваться</button>
            </form>
        </fieldset>
    </div>
</div>