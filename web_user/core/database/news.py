from _datetime import datetime
from web_user.core.database.all_roll import *
from web_user.app import db


class News(db.Model):
    __tablename__ = 'sercks_news'
    id = db.Column(db.BIGINT, primary_key=True)
    title = db.Column(db.VARCHAR(128), nullable=False)
    information = db.Column(db.TEXT, nullable=False)
    theme = db.relationship('ThemeNews', secondary=news_theme,
                            backref=db.backref('news_project', lazy='dynamic'))
    video = db.relationship('Video', backref='News', lazy='dynamic')
    image = db.relationship('Image', backref='News', lazy='dynamic')
    istochnik = db.Column(db.VARCHAR(128))
    data = db.Column(db.DateTime, default=datetime.now())
    admin = db.Column(db.BIGINT, db.ForeignKey('adm_serks_admin.id'))

    def __repr__(self):
        return self.id


#### Разное(тематика, видео, картинки) ####
class ThemeNews(db.Model):
    __tablename__ = 'serkcs_themanews'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(128), nullable=False, unique=True)

    def __repr__(self):
        return self.id


class Image(db.Model):
    id = db.Column(db.BIGINT, primary_key=True)
    href = db.Column(db.VARCHAR(255), nullable=False)
    blogs = db.Column(db.BIGINT, db.ForeignKey('serks_blogs.id'))
    news = db.Column(db.BIGINT, db.ForeignKey('sercks_news.id'))

    def __repr__(self):
        return self.id
