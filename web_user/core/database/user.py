from _datetime import datetime
from web_user.core.database.news import *
from web_user.core.database.all_roll import *
from web_user.app import db


class Administrators(db.Model):
    __tablename__ = 'adm_serks_admin'
    id = db.Column(db.BIGINT, primary_key=True)
    login = db.Column(db.VARCHAR(128), unique=True, nullable=False)
    email = db.Column(db.VARCHAR(128), unique=True, nullable=False)
    password = db.Column(db.VARCHAR(255), nullable=False)
    date_vhod = db.Column(db.DateTime, default=datetime.now())
    role = db.Column(db.BIGINT, db.ForeignKey('adm_serks_roles.id'))
    news = db.relationship('News', backref='Admin', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.id


class RoleAdmin(db.Model):
    __tablename__ = 'adm_serks_roles'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(64), unique=True, nullable=False)
    number_roles = db.Column(db.INTEGER, unique=True, nullable=False, autoincrement=True)
    admins = db.relationship('Administrators', backref='Role', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.id


# Пользователи
class User(db.Model):
    __tablename__ = 'sercks_user'
    id = db.Column(db.BIGINT, primary_key=True)
    login = db.Column(db.VARCHAR(64), nullable=False, unique=True)
    password = db.Column(db.VARCHAR(256), nullable=False)
    email = db.Column(db.VARCHAR(128), nullable=False)
    date_reg = db.Column(db.DateTime, default=datetime.now())
    date_vhod = db.Column(db.DateTime, default=datetime.now())
    info = db.relationship("Information", uselist=False, back_populates="user")
    blogs = db.relationship('Blogs', backref='User', lazy='dynamic')
    videogallerey = db.relationship('VideoGallerey', backref='User', lazy='dynamic')
    delete = db.Column(db.BOOLEAN, default=False)
    black_list = db.Column(db.BOOLEAN, default=False)
    pays = db.relationship('Pays_User', backref='User', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.id


# Города
class City(db.Model):
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(128), unique=True, nullable=False)
    user = db.relationship('Information', backref='City', lazy='dynamic')

    def __repr__(self):
        return '%r' % self.id


class Information(db.Model):
    __tablename__ = 'serks_infousers'
    id = db.Column(db.BIGINT, primary_key=True)

    user_id = db.Column(db.BIGINT, db.ForeignKey('sercks_user.id'))
    user = db.relationship("User", back_populates="info")
    ava = db.Column(db.VARCHAR(128))
    education = db.relationship('EducationUser', backref='serks_infousers', lazy='dynamic')
    # О себе
    info = db.Column(db.TEXT)
    # Номер телефона
    phone = db.Column(db.VARCHAR(64), unique=True)
    # ФИО
    fio = db.Column(db.VARCHAR(128))
    type = db.Column(db.BOOLEAN, default=False)  # Физ лицо True - Юр лицо
    # Место работы
    jobs = db.relationship('JobsUser', backref='serks_infousers', lazy='dynamic')
    city = db.Column(db.BIGINT, db.ForeignKey('city.id'))
    social_networking = db.relationship('SocialNetwork', backref='serks_infousers', lazy='dynamic')

    def __repr__(self):
        return self.id


# Места учебы
class EducationUser(db.Model):
    __tablename__ = 'serks_education'
    id = db.Column(db.BIGINT, primary_key=True)
    institution = db.Column(db.VARCHAR(128), nullable=False)
    diplom = db.Column(db.VARCHAR(128), nullable=False)
    info = db.Column(db.TEXT)
    with_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime)
    user_info = db.Column(db.BIGINT, db.ForeignKey('serks_infousers.id'))

    def __repr__(self):
        return self.id


# Места работы
class JobsUser(db.Model):
    __tablename__ = 'serks_jobsuser'
    id = db.Column(db.BIGINT, primary_key=True)
    name_company = db.Column(db.VARCHAR(128), nullable=False)
    position = db.Column(db.VARCHAR(128), nullable=False)
    achievements = db.Column(db.TEXT)
    with_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime)
    user_info = db.Column(db.BIGINT, db.ForeignKey('serks_infousers.id'))

    def __repr__(self):
        return self.id


# Соц сети
class SocialNetwork(db.Model):
    __tablename__ = 'serks_socialnetworks'
    id = db.Column(db.BIGINT, primary_key=True)
    fb = db.Column(db.VARCHAR(128))
    vk = db.Column(db.VARCHAR(128))
    instagram = db.Column(db.VARCHAR(128))
    gplus = db.Column(db.VARCHAR(128))
    twitter = db.Column(db.VARCHAR(128))
    linkdln = db.Column(db.VARCHAR(128))
    user_info = db.Column(db.BIGINT, db.ForeignKey('serks_infousers.id'))

    def __repr__(self):
        return self.id


# Стена пользователя
class Blogs(db.Model):
    __tablename__ = 'serks_blogs'
    id = db.Column(db.BIGINT, primary_key=True)
    title = db.Column(db.VARCHAR(128), nullable=False)
    theme = db.relationship('ThemeNews', secondary=blogs_theme,
                            backref=db.backref('serkcs_themanews', lazy='dynamic'))
    text = db.Column(db.TEXT, nullable=False)
    video = db.relationship("Video", backref='Blogs', lazy='dynamic')
    image = db.relationship("Image", backref='Blogs', lazy='dynamic')
    date = db.Column(db.DateTime, default=datetime.now())
    user = db.Column(db.BIGINT, db.ForeignKey('sercks_user.id'))

    def __repr__(self):
        return self.id


class ThemeVideoGallerey(db.Model):
    __tablename__ = 'themevideogallerey'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(128), nullable=False)
    info = db.Column(db.TEXT, nullable=False)
    videogallerey = db.relationship("VideoGallerey", backref='ThemeVideoGallerey', lazy='dynamic')
    date = db.Column(db.DateTime, default=datetime.now())
    public = db.Column(db.BOOLEAN, default=False)

    def __repr__(self):
        return self.id


# Для запуска видеоуроков
class VideoGallerey(db.Model):
    __tablename__ = 'serks_videogallerey'
    id = db.Column(db.BIGINT, primary_key=True)
    name = db.Column(db.VARCHAR(128), nullable=False)
    info = db.Column(db.TEXT, nullable=False)
    video = db.relationship("Video", backref='video_education', lazy='dynamic')
    settings = db.relationship("SettingsVideoMaterial", uselist=False, back_populates="videogallerey")
    public = db.Column(db.BOOLEAN, default=False)
    date = db.Column(db.DateTime, default=datetime.now())
    themes = db.Column(db.BIGINT, db.ForeignKey('themevideogallerey.id'))
    user = db.Column(db.BIGINT, db.ForeignKey('sercks_user.id'))

    def __repr__(self):
        return self.id


class Video(db.Model):
    id = db.Column(db.BIGINT, primary_key=True)
    href = db.Column(db.VARCHAR(255), nullable=False)
    blogs = db.Column(db.BIGINT, db.ForeignKey('serks_blogs.id'))
    news = db.Column(db.BIGINT, db.ForeignKey('sercks_news.id'))
    videogallerey = db.Column(db.BIGINT, db.ForeignKey('serks_videogallerey.id'))

    def __repr__(self):
        return self.id


class SettingsVideoMaterial(db.Model):
    __tablename__ = 'settingsvideomaterial'
    id = db.Column(db.BIGINT, primary_key=True)
    public = db.Column(db.BOOLEAN, default=True)
    # Если не публичные то оплати пошлину
    pay_many = db.Column(db.BOOLEAN, default=False)
    theme = db.relationship('ThemeNews', secondary=video_theme,
                            backref=db.backref('settingsvideomaterial', lazy='dynamic'))
    videogallerey_id = db.Column(db.BIGINT, db.ForeignKey('serks_videogallerey.id'))
    videogallerey = db.relationship("VideoGallerey", back_populates="settings")

    def __repr__(self):
        return self.id


# Для админа, подтверждение об оплаты
class Pays_User(db.Model):
    __tablename__ = 'serks_pays'
    id = db.Column(db.BIGINT, primary_key=True)
    cards_nomer = db.Column(db.BIGINT, nullable=False)
    summa = db.Column(db.INTEGER, nullable=False)
    date = db.Column(db.DateTime, default=datetime.now())
    user = db.Column(db.BIGINT, db.ForeignKey('sercks_user.id'))
    # Успешно ли прошла транзакция
    completed = db.Column(db.BOOLEAN, default=False)

    def __repr__(self):
        return self.id
