from web_user.app import db

news_theme = db.Table('news_themanews',
                    db.Column('themanews_id', db.Integer, db.ForeignKey('serkcs_themanews.id')),
                    db.Column('news_id', db.Integer, db.ForeignKey('sercks_news.id'))
                    )

blogs_theme = db.Table('blogs_themanews',
                    db.Column('themanews_id', db.Integer, db.ForeignKey('serkcs_themanews.id')),
                    db.Column('blogs_id', db.Integer, db.ForeignKey('serks_blogs.id'))
                    )

video_theme = db.Table('settingsvideomaterial_themanews',
                    db.Column('themanews_id', db.Integer, db.ForeignKey('serkcs_themanews.id')),
                    db.Column('settingsvideomaterial_id', db.Integer, db.ForeignKey('settingsvideomaterial.id'))
                    )