from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy


class Config_system(object):
    def app(self):
        app = Flask('web_user')
        app.config.from_object('web_user.config.DevelopmentConfig')
        return app

    def db(self, app):
        return SQLAlchemy(app)

    def views(self):
        import web_user.views.views