import tailer
import re
import os

class Ddos_shield(object):
    def __init__(self):
        self.ips = {}
        self.config()
        self.times_file_modify = os.path.getmtime(self.path)
        self.read_log()

    def config(self):
        if os.name == 'nt':
            self.path = os.environ['nginx_logs'] + 'access.log'  # Создать переменную окружения до папки log в windows
        elif os.name == 'unix':
            self.path = '/var/log/nginx/access.log'
        else:
            print('Требуется дописать конфиг для вашей ОСи т.к. пути для нарко ОСей непрописывал')
            exit()

    def read_log(self):
        try:
            for line in tailer.follow(open(self.path)):
                self.lines_read(line)
        except:
            self.time_modify_access()

    def lines_read(self, line):
        m = re.match('^(\d+.\d+.\d+.\d+).*?- 400 0 "-" "-" "-" 0.000--$', line)
        if m:
            print("Blank DoS request: %s" % line)
            ip = self.add_ban_ip(m)
            self.bans(ip, line)
        else:
            self.time_modify_access()

    def add_ban_ip(self, m):
        ip = m.group(1)
        print("ban ip: %s" % ip)
        os.system("ban_ip.sh %s" % ip)
        return ip

    def bans(self, ip, line):
        m_bot = re.match("^(\d+.\d+.\d+.\d+).*?GET /someurl/ .*?$", line) or None

        if m_bot != None:
            ip = m_bot.group(1)

            if self.ips.has_key(ip):
                self.ips[ip] = self.ips[ip] + 1

                if self.ips[ip] > 100:
                    print("ban ip %s" % ip)
                    os.system("ban_ip.sh %s" % ip)
                else:
                    self.ips[ip] = 0
        else:
            self.time_modify_access()

        print("Bot detected request %s ip: %s" % (self.ips[ip], ip))
        print(line)
        self.time_modify_access()

    def time_modify_access(self):
        if os.path.getmtime(self.path) != self.times_file_modify:
            return Ddos_shield()
        else:
            exit()