# -*- coding: utf-8 -*-
import logging


class DB_EXC:
    def __init__(self, signals, code):
        obj = getattr(DB_EXC, signals)
        startx = obj(code)
        return startx

    def err_conn(self, code):
        logging.critical(u'Connection Error %s!!!' % code)
        return 0

    def err_add_rows(self, code):
        logging.error(u'Error add rows %s' % code)
        return 0
