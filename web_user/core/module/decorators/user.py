from functools import wraps

from flask import redirect, session


# Декоратор проверки авторизован ли пользователь
def auth(func):
    @wraps(func)
    def fack(*args, **kwargs):
        if 'adm_key' in session:
            return func(*args, **kwargs)
        else:
            return redirect('/admin')

    return fack