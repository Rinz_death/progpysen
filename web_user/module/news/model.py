from sqlalchemy import exc

from web_user.core.database.news import *


class NewsGlobal(object):
    def views(self, page, maxs):
        print(page)
        print(1)
        try:
            return News.query.order_by(News.data.desc()).paginate(page, maxs, False).items
        except exc.DataError:
            try:
                return News.query.paginate(page, maxs, False).items
            except exc.InternalError:
                kol = News.query.all()
                return News.query.paginate(page, int(len(kol)), False).items
        except exc.InternalError:
            return False

    def edits(self):
        pass

    def del_news(self):
        if int(id) != 0:
            delnews = News.query.filter_by(id=id).one()
            db.session.delete(delnews)
            db.session.commit()
        else:
            return 'Error delete fields'

    def add_news(self, form, multiple=False):
        if self.corrects(form, ['title', 'date', 'text']) == True:
            add_news = News.query(title=form['title'])
            db.session.add(add_news)
            db.session.commit()
        else:
            return 'Error add field'

    def corrects(self, form, attr, sc=0):
        red = len(attr) - sc or False
        if red == False:
            return True
        if str(attr[red]) in form:
            return self.corrects(form, attr, sc=sc + 1)
        else:
            return False

class NewsUser(NewsGlobal):
    def __init__(self):
        pass