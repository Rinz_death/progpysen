import re
from _datetime import datetime
from web_user.app import db
from web_user.core.database.user import User, Administrators
from sqlalchemy.orm import exc
from flask import session


class Verification(object):
    # Авторизация пользователей
    def auth(self, form):
        try:
            user = User.query.filter_by(login=form['login'], password=form['password']).one()
            if user:
                session['x_key'] = user.id
                user.date_vhod = datetime.now()
                db.session.add(user)
                db.session.commit()
                return 'Успешно'
            else:
                return 'Упс, ошибка'
        except exc.NoResultFound:
            return 'Неверно введен почта и/или пароль'

    ########################################
    # def shield(self, password, email, login):
    #     slovar = {'login': [login], 'password': [password], 'email': [email] }
    #     for i in range(100):
    #         non_shield = ['123456', '654321']
    #         shields_pass = re.search(r'', password)
    #         shields_email = re.search(r'', email)
    #         shields_login = re.search(r'', login)
    ########################################
    # Регистрация
    def reg(self, login, password, email):
        try:
            password = int(password)
            return 'Пароль не может состоят только из цифр'
        except ValueError:
            if len(password) >= 6:

                if password != '123456' and password != '654321' and password != login and password != email \
                        and password.find(str(login)) == -1 and password.find(str(email)) == -1:
                    if not bool(email.find(str(login))):
                        try:
                            if bool(User.query.filter_by(login=login).one()) != True and bool(
                                    User.query.filter_by(email=email).one()) != True \
                                    and login.find('admin') == -1 and email.find('admin') == -1 \
                                    and email.find('moderator') == -1:
                                return User(login=str(login), password=str(password), email=str(email))
                            else:
                                return 'Такой логин и/или email уже заняты'
                        except exc.NoResultFound:  # Обработать ошибку которая может возникнуть
                            try:
                                if not bool(User.query.filter_by(email=email).one()):
                                    return User(login=str(login), password=str(password), email=str(email))
                            except exc.NoResultFound:
                                return User(login=str(login), password=str(password), email=str(email))
                    else:
                        return 'Логин не безопасен т.к. его имя является частью вашего email почты'
                else:
                    return 'Небезопасный пароль!'
            else:
                return 'Слишком короткий пароль!'

    # Авторизация администраторов
    def auth_admin(self, form):
        try:
            if '@' in form['email']:
                admin = Administrators.query.filter_by(email=form['email'], password=form['password'],
                                                       role=form['role']).one()
            else:
                admin = Administrators.query.filter_by(login=form['email'], password=form['password'],
                                                       role=form['role']).one()
            if admin:
                admin.date_vhod = datetime.now()
                db.session.add(admin)
                db.session.commit()
                session['adm_key'] = admin.id
                return 'Успешно'
            else:

                return 'Упс, ошибка'
        except exc.NoResultFound:
            return 'Неверно введен почта и/или пароль'
