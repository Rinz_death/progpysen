from flask import render_template, redirect, url_for, jsonify
from web_user.app import app

# api = Api(app)
#
#
# @api.representation('application/json')
# def output_json(data, code, headers=None):
#     resp = make_response(json.dumps(data), code)
#     resp.headers.extend(headers or {})
#     return resp


@app.route('/')
@app.route('/index/<int:page>')
def index(page=0):
    news = NewsUser().views(page,9)
    a = [1,2,3,4,5,7]
    print(a.__getitem__(2))
    print(news)
    return render_template('/index.html', news=news)

from web_user.views.admin.views import *
from web_user.api.user import *