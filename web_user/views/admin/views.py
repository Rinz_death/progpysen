from flask import render_template, redirect, url_for, jsonify, request, session, Response
from web_user.app import app
from web_user.module.news.model import NewsUser
from web_user.module.reg_auth.model import Verification
from web_user.core.module.decorators.user import auth


# Главная страница
@app.route('/admin')
def adm_index():
    if 'adm_key' in session:
        news = NewsUser().views(1, 5)
        return render_template('/admin/index.html', news=news)

    return render_template('/admin/index.html')


# Пост запрос на авторизацию
@app.route('/admin', methods=['POST'])
def adm_index_post():
    verificatys = Verification().auth_admin(request.form)
    return render_template('/admin/index.html', auth=verificatys)


# Выход
@app.route('/admin/exit')
@auth
def exits():
    if session['adm_key']:
        session.clear()
    return redirect('/')


@app.route('/admin/settings/news')
@auth
def admin_news_add():
    news = NewsUser().views(1, 5)
    return render_template('/admin/news.html', news=news)

@app.route('/admin/add_news', methods=['POST'])
def add_news_admin():

    return redirect('/admin/settings/news')

@app.route('/admin/info/statistics')
@auth
def admin_statics():
    return render_template('/admin/statistics.html')