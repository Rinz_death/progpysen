class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'bmp'])
    SQLALCHEMY_DATABASE_URI = 'postgresql://Rider:930114@localhost:5432/projects_async'  # Подключаем базу postgres
    # DATABASE = {
    #     'name': 'projects_async',
    #     'engine': 'peewee.PostgresqlDatabase',
    #     'user': 'Rider',
    #     'password': '930114',
    # }

class ProductionConfig(Config):
    DEBUG = False

class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True

class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False

class TestingConfig(Config):
    TESTING = True
# from web_user.core.database.user import *