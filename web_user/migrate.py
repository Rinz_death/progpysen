# from playhouse.migrate import *
from peewee_migrate import Router
from peewee import PostgresqlDatabase
from web_user.app import db

def migrate():
    print(db)
    router = Router(PostgresqlDatabase(db))

    # Create migration
    router.create('migration_name')

    # Run migration/migrations
    router.run('migration_name')

    # Run all unapplied migrations
    router.run()

