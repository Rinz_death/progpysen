#! \env\framework_async\Scripts\python
# coding=utf-8
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from web_user.app import app, db

app.config.from_object('web_user.config.DevelopmentConfig')

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()

#\env\framework_async\Scripts\python migrations.py db upgrade
